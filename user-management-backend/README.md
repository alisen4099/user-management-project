# How to run
install dependencies
``` bash
go mod tidy
```
run the server
``` bash
go run main.go
```