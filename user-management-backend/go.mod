module user-management-backend

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/stretchr/testify v1.8.4
)

require github.com/felixge/httpsnoop v1.0.1 // indirect

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
