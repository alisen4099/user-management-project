package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"user-management-backend/models"
	"user-management-backend/service"

	"github.com/gorilla/mux"
)

type UserHandler struct {
	UserService service.UserService
}

// GetUsers handles GET request for all users
func (h *UserHandler) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := h.UserService.GetUsers()
	if err != nil {
		if err.Error() == "No users found" {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	json.NewEncoder(w).Encode(users)
}

// GetUserByID handles GET request for a user by ID
func (h *UserHandler) GetUserByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	user, err := h.UserService.GetUserByID(id)
	if err != nil {
		if err.Error() == "User not found" {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	json.NewEncoder(w).Encode(user)
}

// CreateUser handles POST request for creating a user
func (h *UserHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	//validate json
	var tempMap map[string]interface{}
	var user models.User

	err := json.NewDecoder(r.Body).Decode(&tempMap)
	if err != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}

	// Check for extra fields
	for key := range tempMap {
		if key != "name" && key != "email" {
			http.Error(w, "Invalid field: "+key, http.StatusBadRequest)
			return
		}
	}

	// Now decode to your User struct
	byteData, err := json.Marshal(tempMap)
	if err != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}
	json.Unmarshal(byteData, &user)

	json.NewDecoder(r.Body).Decode(&user)
	newUser, err := h.UserService.CreateUser(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(newUser)
}

// UpdateUser handles PUT request for updating a user
func (h *UserHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	//validate json
	var tempMap map[string]interface{}
	var user models.User

	err2 := json.NewDecoder(r.Body).Decode(&tempMap)
	if err2 != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}

	// Check for extra fields
	for key := range tempMap {
		if key != "name" && key != "email" {
			http.Error(w, "Invalid field: "+key, http.StatusBadRequest)
			return
		}
	}

	// Now decode to your User struct
	byteData, err := json.Marshal(tempMap)
	if err != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}
	json.Unmarshal(byteData, &user)

	json.NewDecoder(r.Body).Decode(&user)
	updatedUser, err := h.UserService.UpdateUser(id, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(updatedUser)
}

// DeleteUser handles DELETE request for deleting a user
func (h *UserHandler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}

	err = h.UserService.DeleteUser(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
