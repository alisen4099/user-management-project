package handler_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	handler "user-management-backend/handlers"
	"user-management-backend/models"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

// Mock your service methods here.
type mockUserService struct{}

func (m *mockUserService) GetUsers() ([]models.User, error)                  { return nil, nil }
func (m *mockUserService) GetUserByID(id int) (*models.User, error)          { return nil, nil }
func (m *mockUserService) CreateUser(user models.User) (*models.User, error) { return &user, nil }
func (m *mockUserService) UpdateUser(id int, user models.User) (*models.User, error) {
	return &user, nil
}
func (m *mockUserService) DeleteUser(id int) error { return nil }

func TestCreateUser(t *testing.T) {
	h := handler.UserHandler{
		UserService: &mockUserService{},
	}

	// Test for creating user successfully
	reqBody, _ := json.Marshal(map[string]string{
		"name":  "John",
		"email": "john@example.com",
	})
	req := httptest.NewRequest("POST", "/users", bytes.NewBuffer(reqBody))
	resp := httptest.NewRecorder()

	h.CreateUser(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	// Test for 400 Bad Request with extra field
	reqBody, _ = json.Marshal(map[string]string{
		"name":  "John",
		"email": "john@example.com",
		"age":   "25", // extra field
	})
	req = httptest.NewRequest("POST", "/users", bytes.NewBuffer(reqBody))
	resp = httptest.NewRecorder()

	h.CreateUser(resp, req)

	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestUpdateUser(t *testing.T) {
	h := handler.UserHandler{
		UserService: &mockUserService{},
	}

	router := mux.NewRouter()
	router.HandleFunc("/users/{id:[0-9]+}", h.UpdateUser).Methods("PUT")

	// Test for updating user successfully
	reqBody, _ := json.Marshal(map[string]string{
		"name":  "John",
		"email": "john@example.com",
	})
	req := httptest.NewRequest("PUT", "/users/1", bytes.NewBuffer(reqBody))
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	// Test for 400 Bad Request with extra field
	reqBody, _ = json.Marshal(map[string]string{
		"name":  "John",
		"email": "john@example.com",
		"age":   "25", // extra field
	})
	req = httptest.NewRequest("PUT", "/users/1", bytes.NewBuffer(reqBody))
	resp = httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusBadRequest, resp.Code)
}
