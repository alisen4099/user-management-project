package main

import (
	"fmt"
	"net/http"
	"user-management-backend/database"
	handler "user-management-backend/handlers"
	"user-management-backend/repository"
	"user-management-backend/service"

	"github.com/gorilla/handlers"

	"github.com/gorilla/mux"
)

// Dummy function just to test the HTTP server.
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
}

func main() {
	r := mux.NewRouter()
	db := database.Connect()
	userRepo := repository.NewUserRepo(db)
	userService := service.NewUserService(userRepo)
	userHandler := &handler.UserHandler{UserService: userService}

	r.HandleFunc("/users", userHandler.GetUsers).Methods("GET")
	r.HandleFunc("/users/{id:[0-9]+}", userHandler.GetUserByID).Methods("GET")
	r.HandleFunc("/users", userHandler.CreateUser).Methods("POST")
	r.HandleFunc("/users/{id:[0-9]+}", userHandler.UpdateUser).Methods("PUT")
	r.HandleFunc("/users/{id:[0-9]+}", userHandler.DeleteUser).Methods("DELETE")

	// CORS setup
	corsHandler := handlers.CORS(
		handlers.AllowedOrigins([]string{"http://localhost:3000"}), // Adjust this to your frontend URL
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}),
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
	)

	http.Handle("/", r)
	fmt.Println("Server started at port 8080")
	http.ListenAndServe(":8080", corsHandler(r))
}
