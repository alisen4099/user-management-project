package repository

import (
	"database/sql"
	"user-management-backend/models"
)

// UserRepository represents the contract for user repository
type UserRepository interface {
	GetUsers() ([]models.User, error)
	GetUserByID(id int) (*models.User, error)
	CreateUser(user models.User) (*models.User, error)
	UpdateUser(id int, user models.User) (*models.User, error)
	DeleteUser(id int) error
}

// NewUserRepo initializes a new UserRepository
func NewUserRepo(DB *sql.DB) UserRepository {
	return &UserRepositoryImpl{DB}
}

// UserRepositoryImpl implements UserRepository
type UserRepositoryImpl struct {
	DB *sql.DB
}

// GetUsers returns all users from the database
func (repo *UserRepositoryImpl) GetUsers() ([]models.User, error) {
	rows, err := repo.DB.Query("SELECT id, name, email FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.ID, &user.Name, &user.Email); err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}

// GetUserByID gets a single user by ID
func (repo *UserRepositoryImpl) GetUserByID(id int) (*models.User, error) {
	var user models.User
	err := repo.DB.QueryRow("SELECT id, name, email FROM users WHERE id=$1", id).Scan(&user.ID, &user.Name, &user.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return &user, nil
}

// CreateUser adds a new user to the database
func (repo *UserRepositoryImpl) CreateUser(user models.User) (*models.User, error) {
	err := repo.DB.QueryRow("INSERT INTO users(name, email) VALUES($1, $2) RETURNING id", user.Name, user.Email).Scan(&user.ID)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// UpdateUser updates the user with the given ID
func (repo *UserRepositoryImpl) UpdateUser(id int, user models.User) (*models.User, error) {
	_, err := repo.DB.Exec("UPDATE users SET name=$1, email=$2 WHERE id=$3", user.Name, user.Email, id)
	if err != nil {
		return nil, err
	}

	return repo.GetUserByID(id)
}

// DeleteUser deletes the user with the given ID
func (repo *UserRepositoryImpl) DeleteUser(id int) error {
	_, err := repo.DB.Exec("DELETE FROM users WHERE id=$1", id)
	if err != nil {
		return err
	}
	return nil
}
