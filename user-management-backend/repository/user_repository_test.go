package repository_test

import (
	"testing"
	"user-management-backend/database"
	"user-management-backend/models"
	"user-management-backend/repository"

	"github.com/stretchr/testify/assert"
)

func TestUserRepository(t *testing.T) {

	// Connect to the database
	db := database.Connect()
	userRepo := repository.UserRepositoryImpl{DB: db}

	// Initialize a sample user
	sampleUser := models.User{
		Name:  "John Doe",
		Email: "johndoe@example.com",
	}

	// CREATE: Add user
	t.Run("Create User", func(t *testing.T) {
		newUser, err := userRepo.CreateUser(sampleUser)
		assert.Nil(t, err)
		assert.NotNil(t, newUser)
		sampleUser.ID = newUser.ID // Set ID for subsequent tests
	})

	// READ: Get user by ID
	t.Run("Get User by ID", func(t *testing.T) {
		foundUser, err := userRepo.GetUserByID(sampleUser.ID)
		assert.Nil(t, err)
		assert.NotNil(t, foundUser)
		assert.Equal(t, sampleUser.Name, foundUser.Name)
		assert.Equal(t, sampleUser.Email, foundUser.Email)
	})

	// READ: Get all users
	t.Run("Get All Users", func(t *testing.T) {
		users, err := userRepo.GetUsers()
		assert.Nil(t, err)
		assert.NotNil(t, users)
		assert.True(t, len(users) > 0)
	})

	// UPDATE: Update user
	t.Run("Update User", func(t *testing.T) {
		updatedUser := models.User{
			Name:  "John Updated",
			Email: "johnupdated@example.com",
		}
		user, err := userRepo.UpdateUser(sampleUser.ID, updatedUser)
		assert.Nil(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, "John Updated", user.Name)
		assert.Equal(t, "johnupdated@example.com", user.Email)
	})

	// DELETE: Delete user
	t.Run("Delete User", func(t *testing.T) {
		err := userRepo.DeleteUser(sampleUser.ID)
		assert.Nil(t, err)
	})
}
