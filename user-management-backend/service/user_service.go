package service

import (
	"errors"
	"user-management-backend/models"
	"user-management-backend/repository"
)

type UserService interface {
	GetUsers() ([]models.User, error)
	GetUserByID(id int) (*models.User, error)
	CreateUser(user models.User) (*models.User, error)
	UpdateUser(id int, user models.User) (*models.User, error)
	DeleteUser(id int) error
}

func NewUserService(repo repository.UserRepository) UserService {
	return &UserServiceImpl{
		UserRepo: repo,
	}
}

type UserServiceImpl struct {
	UserRepo repository.UserRepository
}

func (s *UserServiceImpl) GetUsers() ([]models.User, error) {
	users, err := s.UserRepo.GetUsers()
	if err != nil {
		return nil, err
	}
	if len(users) == 0 {
		return nil, errors.New("No users found")
	}
	return users, nil
}

func (s *UserServiceImpl) GetUserByID(id int) (*models.User, error) {
	user, err := s.UserRepo.GetUserByID(id)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, errors.New("User not found")
	}
	return user, nil
}

func (s *UserServiceImpl) CreateUser(user models.User) (*models.User, error) {
	return s.UserRepo.CreateUser(user)
}

func (s *UserServiceImpl) UpdateUser(id int, user models.User) (*models.User, error) {
	return s.UserRepo.UpdateUser(id, user)
}

func (s *UserServiceImpl) DeleteUser(id int) error {
	return s.UserRepo.DeleteUser(id)
}
