import React, { useState, useEffect } from 'react'; // Import useEffect
import axios from 'axios';
import UserGrid from './UserGrid';
import UserDetails from './UserDetails';

function App() {
  const [action, setAction] = useState(null);
  const [user, setUser] = useState(null);
  const [refresh, setRefresh] = useState(false); // State for tracking refreshes

  useEffect(() => {
    if (refresh) {
      setRefresh(false); // Reset the refresh flag
    }
  }, [refresh]);

  const handleNew = () => {
    setAction("New");
    setUser({ name: "", email: "" });
  };

  const handleEdit = (userId) => {
    axios
      .get(`http://localhost:8080/users/${userId}`)
      .then((response) => {
        setUser(response.data);
        setAction("Edit");
      })
      .catch((error) => {
        console.error("There was an error fetching the data", error);
      });
  };

  const handleDelete = (userId) => {
    axios
      .delete(`http://localhost:8080/users/${userId}`)
      .then(() => {
        setRefresh(true); // Trigger a refresh after deleting
        setAction(null);
        setUser(null);
      })
      .catch((error) => {
        console.error("There was an error deleting the data", error);
      });
  };

  const handleSave = (user) => {
    let apiEndpoint;
    let apiMethod;

    if (action === 'New') {
      apiEndpoint = 'http://localhost:8080/users';
      apiMethod = 'post';
    } else if (action === 'Edit') {
      apiEndpoint = `http://localhost:8080/users/${user.id}`;
      apiMethod = 'put';
    } else if (action === 'Delete') {
      apiEndpoint = `http://localhost:8080/users/${user.id}`;
      apiMethod = 'delete';
    }

    axios[apiMethod](apiEndpoint, user)
      .then((response) => {
        setRefresh(true); // Trigger a refresh after saving
        console.log('User data saved:', response);
      })
      .catch((error) => {
        console.log('There was an error saving the data:', error);
      });

    setAction(null); // Resetting the action
  };

  const handleBack = () => {
    setAction(null);
    setUser(null);
  };

  return (
    <div>
      <UserGrid onNew={handleNew} onEdit={handleEdit} onDelete={handleDelete} />
      {action && (
        <UserDetails
          user={user}
          onSave={handleSave}
          onBack={handleBack}
          action={action}
        />
      )}
    </div>
  );
};

export default App;
