import React, { useState, useEffect } from "react"; // Import useEffect
import "./UserDetails.css"; // Import the CSS

const UserDetails = ({ user, onSave, onBack, action }) => {
  const [localUser, setLocalUser] = useState(user);

  useEffect(() => {
    setLocalUser(user);  // Update localUser whenever user changes
  }, [user]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setLocalUser((prevUser) => ({ ...prevUser, [name]: value }));
  };

  const handleAction = () => {
    onSave(localUser);
    window.location.reload();

  };

  const handleBack = () => {
    onBack();
  };

  const getActionText = () => {
    const mapping = {
      "New": "Create",
      "Edit": "Save",
      "Delete": "Delete",
    };
    return mapping[action];
  };

  return (
    <div className="user-details-modal">
      <div className="user-details-content">
        <h2>User Details</h2>
        <form>
          <div>
            <label>Name: </label>
            <input 
              type="text"
              name="name"
              value={localUser.name}
              onChange={handleInputChange}
              disabled={action === "Delete"}
            />
          </div>
          <div>
            <label>Email: </label>
            <input 
              type="email"
              name="email"
              value={localUser.email}
              onChange={handleInputChange}
              disabled={action === "Delete"}
            />
          </div>
          <div style={{ margin: "20px" }}>
            <button type="button" onClick={handleAction}>
              {getActionText()}
            </button>
            <button type="button" onClick={handleBack} style={{ marginLeft: "10px" }}>
              Back
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default UserDetails;
