import React, { useState, useEffect } from "react";
import axios from "axios";

const UserGrid = ({ onNew, onEdit, onDelete }) => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  useEffect(() => {
    axios
      .get("http://localhost:8080/users")
      .then((response) => {
        setUsers(response.data);
      })
      .catch((error) => {
        console.error("There was an error fetching the data", error);
      });
  }, []);

  const handleNew = () => {
    onNew();
  };

  const handleEdit = () => {
    onEdit(selectedUser);
  };

  const handleDelete = () => {
    onDelete(selectedUser);
    window.location.reload();

  };

  return (
    <div style={{ margin: "20px", textAlign: "center" }}>
      <h1>User Management</h1>
      <div>
        <button 
          style={{ margin: "10px", padding: "10px 20px" }} 
          onClick={handleNew}
        >
          New
        </button>
        <button 
          style={{ margin: "10px", padding: "10px 20px" }} 
          onClick={handleEdit} 
          disabled={!selectedUser}
        >
          Edit
        </button>
        <button 
          style={{ margin: "10px", padding: "10px 20px" }} 
          onClick={handleDelete} 
          disabled={!selectedUser}
        >
          Delete
        </button>
      </div>
      <div style={{ margin: "20px" }}>
        <table style={{ margin: "auto", width: "60%", borderCollapse: "collapse" }}>
          <thead>
            <tr>
              <th style={{ border: "1px solid black", padding: "10px" }}>ID</th>
              <th style={{ border: "1px solid black", padding: "10px" }}>Name</th>
              <th style={{ border: "1px solid black", padding: "10px" }}>Email</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr
                key={user.id}
                onClick={() => setSelectedUser(user.id)}
                style={{
                  backgroundColor: selectedUser === user.id ? "#ccc" : "white",
                  cursor: "pointer",
                  border: "1px solid black",
                }}
              >
                <td style={{ border: "1px solid black", padding: "10px" }}>{user.id}</td>
                <td style={{ border: "1px solid black", padding: "10px" }}>{user.name}</td>
                <td style={{ border: "1px solid black", padding: "10px" }}>{user.email}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default UserGrid;
